
const express = require('express');

const app = express();
const PORT = 3000;
const hostname = '127.0.0.1';

app.use(express.json()); 

app.listen(PORT, () => console.log(` server  ${hostname} ${PORT}`));


//////////////////////////////////////////////////////////////



let fs = require('fs');                
const lineReader = require('readline');  

let Domain = "http://127.0.0.1:3000/"
let Chemindufichier = "resources/mots.txt";
let minMot = 3;
let maxMot = 6;

let mots = []
let urls = {}

function recupMots(callback) {
    if(mots.length > 0) {
        return mots; 
    }

    let rl = lineReader.createInterface({
        input: fs.createReadStream(Chemindufichier)
    });

    rl.on('line', function (line) {
        let LongueurduMot = line.length;
        if (LongueurduMot >= minMot && LongueurduMot <= maxMot)
            mots.push(line);
    });

    rl.on('close', function (line) {
        callback();
    });
}

function shortenURL(url) {
    let randInt = getRandomInt(0, mots.length - 1);
    let randMot = mots.splice(randInt, 1)[0];

    urls[randMot] = url;

    return Domain + randMot;
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getURLFromShortened(shortenedURL) {
    return urls[shortenedURL.replace(Domain, "")];
}

function removeShortenedURL(shortenedURL) {
    let mot = shortenedURL.replace(Domain, "");
    delete urls[mot];
    mots.push(mot);
}


function startServer() {
    
    recupMots(function() {

        console.log("Mots disponibles: " + mots.length);
        let newURL = shortenURL("https://kruikube.alwaysdata.net/");
        console.log("New URL: " + newURL);
        let oldURL = getURLFromShortened(newURL);
        console.log("Old URL: " + oldURL);
        removeShortenedURL(newURL);    
    });
      
}
     
startServer()
